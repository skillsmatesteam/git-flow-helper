#!/bin/bash
#
# Cyrille MOFFO
#
# Script de gestion des modules du projet skillsmates
#

# Conf.
GIT_FLOW_CONF=git-flow-helper.conf
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/skillsmates 2>&1 && pwd )"
if [ -f $GIT_FLOW_CONF ]; then
  . ${GIT_FLOW_CONF}
else
  echo "Error : config file ${GIT_FLOW_CONF} is missing"
  exit 1
fi

function foreach_project_do() {
    for project in $PROJECTS
    do
      echo "***** $project *****"
      cd "$ROOT_DIRECTORY/$project"
      echo $1
      $1
      cd -
      echo "  "
    done
}

function foreach_ms_module_do() {
    for module in $MS_MODULES
    do
      echo "***** $module *****"
      cd "$ROOT_DIRECTORY/ms-skillsmates-parent/$module"
      echo $1
      $1
      cd -
      echo "  "
    done
}

function foreach_lib_module_do() {
    for module in $LIB_MODULES
    do
      echo "***** $module *****"
      cd "$ROOT_DIRECTORY/lib-skillsmates-parent/$module"
      echo $1
      $1
      cd -
      echo "  "
    done
}

function switch_branch() {
    echo $1
    if [ -z "$1" ]
    then
      exit 2
    fi
    foreach_project_do "git checkout $1"
}

function gitflow_init() {
    foreach_project_do "git flow init -d"
}

function ls_local_branch() {
    foreach_project_do "git branch --list"
}

function ls_remote_branch() {
    foreach_project_do "git remote update origin --prune"
    foreach_project_do "git branch -r --list"
}

function ls_tags() {
#    foreach_project_do "git ls-remote --tags origin"
    foreach_project_do "git tag"
}

function ls_remote_tags() {
    foreach_project_do "git ls-remote --tags origin"
}

function status() {
    foreach_project_do "git status"
}

function pull_master() {
    foreach_project_do "git remote update origin --prune"
    switch_branch "master"
    foreach_project_do "git pull origin master"
}

function pull_develop() {
    switch_branch "develop"
    foreach_project_do "git pull origin develop"
}

function push_master() {
    switch_branch "master"
    foreach_project_do "git push origin master"
}

function push_develop() {
    switch_branch "develop"
    foreach_project_do "git push origin develop"
}

function start_feature() {
    pull_develop
    foreach_project_do "git flow feature start $1"
}

function finish_feature() {
    switch_branch "develop"
    pull_develop
    foreach_project_do "git flow feature finish $1"
    push_develop
}

function publish_feature() {
    foreach_project_do "git flow feature publish $1"
}

function pull_feature() {
    foreach_project_do "git flow feature track $1"
}

function start_release() {
    switch_branch "develop"
    foreach_project_do "git flow release start $1"
}

function finish_release() {
    foreach_project_do "git remote update origin --prune"
    pull_master
    pull_develop
    foreach_project_do "git flow release finish --push $1"
    push_master
    push_develop
}

function publish_release() {
    foreach_project_do "git flow release publish $1"
}

function pull_release() {
    echo "Pulling release: $1"
    if [ -z "$1" ]
    then
      echo "Release: $1 does not exist"
      exit 2
    fi
    switch_branch "release/$1"
    foreach_project_do "git pull origin release/$1"
}

function start_hotfix() {
    foreach_project_do "git remote update origin --prune"
    pull_master
    pull_develop
    foreach_project_do "git flow hotfix start $1"
}

function finish_hotfix() {
    foreach_project_do "git remote update origin --prune"
    pull_master
    pull_develop
    foreach_project_do "git flow hotfix finish $1"
    push_master
    push_develop
}

function publish_hotfix() {
    foreach_project_do "git flow hotfix publish $1"
}

function pull_hotfix() {
    echo "Pulling hotfix: $1"
    if [ -z "$1" ]
    then
      echo "Hotfix: $1 does not exist"
      exit 2
    fi
    switch_branch "hotfix/$1"
    foreach_project_do "git pull origin hotfix/$1"
}

function delete_tag() {
    foreach_project_do "git push --delete origin $1"
    foreach_project_do "git tag -d $1"
}

function build_pro_skillsmates_app() {
    cd "$PRO_SKILLSMATES_APP_DIR"
    ng build --prod --base-href /
}

function build_www_skillsmates_app() {
    cd "$WWW_SKILLSMATES_APP_DIR"
    ng build --prod --base-href /
}

function build_ms_modules() {
    foreach_lib_module_do "mvn clean install -P$1"
    foreach_ms_module_do "mvn clean install -P$1"
}

function deploy_ms_to_azure() {
    build_ms_modules "prod"
    foreach_ms_module_do "mvn azure-webapp:deploy"
}

function git_flow_init() {
    cd "$ROOT_DIRECTORY/$1"
    git flow init -d
    git add .
    git commit -m "init project $1"
    git remote add origin $2
    git pull origin master --allow-unrelated-histories
    git checkout develop
    git push -u origin develop
}

function delete_local_branch() {
    foreach_project_do "git branch -D $1"
}

function delete_remote_branch() {
    foreach_project_do "git push origin --delete $1"
}

function delete_remote_tag() {
    foreach_project_do "git push --delete origin $1"
}

# List of options.
function options() {
  clear
  echo ""
  echo "       1 -  Start Feature"
  echo "       2 -  Finish Feature"
  echo "       3 -  Publish Feature"
  echo "       4 -  Pull Feature"
  echo "       5 -  Start Release"
  echo "       6 -  Finish Release"
  echo "       7 -  Publish Release"
  echo "       8 -  Pull Release"
  echo "       9 -  Start Hotfix"
  echo "       10 -  Finish Hotfix"
  echo "       11 -  Publish Hotfix"
  echo "       12 -  Pull Hotfix"
  echo ""
  echo "       13 -  Status"
  echo "       14 -  Ls Local Branches"
  echo "       15 -  Ls Remote Branches"
  echo "       16 -  Pull Master"
  echo "       17 -  Pull Develop"
  echo "       18 -  Switch Branch"
  echo "       19 -  Delete tag"
  echo ""
  echo "       20 -  Build pro-skillsmates-app"
  echo "       21 -  Build www-skillsmates-app"
  echo "       22 -  Build ms modules"
  echo "       23 -  Deploy MS to Azure"
  echo ""
  echo "       24 -  Git flow init project"
  echo ""
  echo "       25 -  Delete local branch"
  echo "       26 -  Delete remote branch"
  echo "       27 -  Delete remote tag"
  echo ""
  echo "       0 -  Exit"
  echo ""
}

# Menu.
function menuHelper() {
  options
  read -p "     ==> Your choice  :  " choice
  clear
  case "$choice" in
  1)
    read -p "     ==> Start Feature: name  :  " parameter
    start_feature $parameter
    ;;
  2)
    ls_local_branch
    read -p "     ==> finish Feature: name  :  " parameter
    finish_feature $parameter
    ;;
  3)
    ls_local_branch
    read -p "     ==> Publish Feature: name  :  " parameter
    publish_feature $parameter
    ;;
  4)
    ls_remote_branch
    read -p "     ==> Pull Feature: name  :  " parameter
    pull_feature $parameter
    ;;
  5)
    read -p "     ==> Start Release: name  :  " parameter
    start_release $parameter
    ;;
  6)
    ls_local_branch
    read -p "     ==> finish Release: name  :  " parameter
    finish_release $parameter
    ;;
  7)
    ls_local_branch
    read -p "     ==> Publish Release: name  :  " parameter
    publish_release $parameter
    ;;
  8)
    ls_remote_branch
    read -p "     ==> Pull Release: name  :  " parameter
    pull_release $parameter
    ;;
  9)
    read -p "     ==> Start Hotfix: name  :  " parameter
    start_hotfix $parameter
    ;;
  10)
    ls_local_branch
    read -p "     ==> finish Hotfix: name  :  " parameter
    finish_hotfix $parameter
    ;;
  11)
    ls_local_branch
    read -p "     ==> Publish Hotfix: name  :  " parameter
    publish_hotfix $parameter
    ;;
  12)
    ls_remote_branch
    read -p "     ==> Pull Hotfix: name  :  " parameter
    pull_hotfix $parameter
    ;;
  13)
    status
  ;;
  14)
    ls_local_branch
  ;;
  15)
    ls_remote_branch
  ;;
  16)
    pull_master
  ;;
  17)
    pull_develop
  ;;
  18)
    ls_local_branch
    read -p "     ==> Switch Branch: name  :  " parameter
    switch_branch $parameter
  ;;
  19)
    ls_tags
    read -p "     ==> Tag to delete: name  :  " parameter
    delete_tag $parameter
  ;;
  20)
    build_pro_skillsmates_app
  ;;
  21)
    build_www_skillsmates_app
  ;;
  22)
    read -p "     ==> Profile: name  :  " parameter
    build_ms_modules $parameter
  ;;
  23)
    deploy_ms_to_azure
  ;;
  24)
    read -p "     ==> Local project: name  :  " local
    read -p "     ==> Remote repository: name  :  " remote
    git_flow_init $local $remote
  ;;
  25)
    ls_local_branch
    read -p "     ==> Delete local Branch: name  :  " parameter
    delete_local_branch $parameter
    ;;
  26)
    ls_remote_branch
    read -p "     ==> Delete remote Branch: name  :  " parameter
    delete_remote_branch $parameter
    ;;
  27)
    ls_remote_tags
    read -p "     ==> Delete remote tag: name  :  " parameter
    delete_remote_tag $parameter
    ;;
  0)
    exit 0
    ;;
  *)
    echo "Invalid choice"
    menuHelper
    ;;
  esac
  read -p "Press [Enter] to continue... "
}


# Logo.
function logo() {
  clear
  echo ""
  echo ""
  echo "        .__  __      _____.__                  .__           .__                          "
  echo "   ____ |__|/  |_  _/ ____\  |   ______  _  __ |  |__   ____ |  | ______   ___________    "
  echo "  / ___\|  \   __\ \   __\|  |  /  _ \ \/ \/ / |  |  \_/ __ \|  | \____ \_/ __ \_  __ \   "
  echo " / /_/  >  ||  |    |  |  |  |_(  <_> )     /  |   Y  \  ___/|  |_|  |_> >  ___/|  | \/   "
  echo " \___  /|__||__|    |__|  |____/\____/ \/\_/   |___|  /\___  >____/   __/ \___  >__|      "
  echo "/_____/                                             \/     \/     |__|        \/          "
  echo "                                                                                          "
  echo ""
}


# ---------------------------------------------------
# ------------------- MAIN ----------------------
# ---------------------------------------------------

if [ $1 == "menu" ]; then
  logo
  while true
  do
    clear
    menuHelper
  done
else
  echo "Invalid choice ..., Enter 'menu' as a parameter"
fi

clear
echo "Finished ..."
exit 0
